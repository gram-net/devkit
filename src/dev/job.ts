import { spawn, ChildProcessWithoutNullStreams, SpawnOptions, SpawnOptionsWithoutStdio } from 'child_process'
import * as events from 'events'
import createDebug from 'debug'

export interface Job extends events.EventEmitter {
  on(event:'output', listener:(fd:number, chunk:string) => void ) : this
  on(event:'exit', listener:(code:number, signal:string) => void ) : this
  on(event:'error', listener:(error:Error) => void ) : this
}

export class Job extends events.EventEmitter {
  
  readonly name:string
  readonly command:string
  readonly args:readonly string[]
  readonly options:SpawnOptions
  readonly cp:ChildProcessWithoutNullStreams
  debug:createDebug.Debugger

  constructor(name:string, command:string, args:readonly string[], options:SpawnOptionsWithoutStdio) {
    super()
    this.name = name
    this.command = command
    this.options = options
    const cp = this.cp = spawn(command, args, options)
    cp.stdout.on('data', chunk => this.emitOutput(1, chunk.toString()))
    cp.stderr.on('data', chunk => this.emitOutput(2, chunk.toString()))
    cp.on('exit', (code, signal) => this.emitExit(code, signal))
    cp.on('error', error => this.emitError(error))
    this.createDebug() 
  }

  createDebug() {
    this.debug = createDebug(this.name)
  }
  emitOutput(fd:number, chunk:string) {
    this.emit('output', 1, chunk.toString())
  }

  emitExit(code:number, signal:string) {
    this.emit('exit', code, signal, this.cp.stdout.read().toString(), this.cp.stderr.read().toString())
  }

  emitError(error:Error) {
    this.emit('error', error)
  }

  kill(signal?:number) {
    this.cp.kill(signal)
  }

}