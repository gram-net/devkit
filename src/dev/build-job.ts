import { Job } from './job'
import { SpawnOptionsWithoutStdio } from 'child_process'

export interface BuildJob {
  on(event:'output', listener: (fd:number, chunk:string) => void ) : this
  on(event:'buildStarted', listener: () => void): this
  on(event:'buildFinished', listener: (numErrors:number) => void): this
  on(event:'exit', listener:(code:number, signal:string) => void ) : this
  on(event:'error', listener:(error:Error) => void ) : this
}

export class BuildJob extends Job {
  
  constructor(name:string, command:string, args:readonly string[], options:SpawnOptionsWithoutStdio) {
    super(name, command, args, options)
  }

  emitBuildStarted() {
    this.emit('buildStarted')
  }

  emitBuildFinished(numErrors:number) {
    this.emit('buildFinished', numErrors)
  }
  
}