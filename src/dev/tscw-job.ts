import { BuildJob } from './build-job'

const NOOP = ()=>{}

export class TscwJob extends BuildJob {
  
  hasErrors:boolean
  
  constructor(name:string, cwd:string) {
    super(`${name}`, 'tsc', ['-w'], {cwd:cwd})
  }

  emitOutput(fd:number, chunk:string) {
    if(fd===1) {
      if(chunk === '\x1bc') return
      let match = chunk.match(/Starting incremental compilation/)
      if(match) {
        this.emitBuildStarted()
        super.emitOutput(1, chunk)
      } else {
        match = chunk.match(/Found ([0-9]*) error/)
        if(match) { 
          process.nextTick(()=>this.emitBuildFinished(Number(match[1]))) 
        }
        super.emitOutput(1, chunk)
      }
    } else {
      console.log({fd, chunk})
      throw new Error('unexpected output on fd=2')
    }
  }

}