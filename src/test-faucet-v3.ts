import { AAccount, Environment, LiteClient, contracts, createDebug, Signer } from './sdk/njs'
import { Account as WalletAccount } from './sdk/com/contracts/wallet'
import nacl from 'tweetnacl'

const {FaucetV3, Wallet} = contracts
const debug = createDebug('test')
const lc = new LiteClient
const env = new Environment(lc)

Environment.handleUnhandledRejections()
run()

const ADDR_FS1 = '0f93d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3dwi2'
const BADDR_FS1 = 'kf93d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d1Vz'
const SADDR_FS1 = '-1:7777777777777777777777777777777777777777777777777777777777777777'

async function run() {
  let r, addr
  const wallet = new Wallet.Contract(env) // wallet contract
  
  /*
    Following loads default signers from sandbox/faucet-s1-master.seed and sandbox/faucet-s1-bot.seed
    You could replace signers.bot and/or signers.master with your own signers
  */
  const signers = await FaucetV3.Util.loadSigners()

  const config = await FaucetV3.Util.loadConfig()
  
  const f1 = await FaucetV3.createAccount(env, ADDR_FS1)
  const main = await wallet.load(-1)
  const allAccounts:Record<string,AAccount> = {main}

  f1.signer = signers.master

  await giftAndDeploy({main}, 555)

  debug('Resetting...')
  await f1.sendOp_Reset()
  
  debug('Sending msg from main to become 1st sponsor...')
  await main.send(f1.naddr, 0.333)
  await showUsers()
  await showInfo({f1, main})
  
  await doSponsors()
  await showUsers()
  await showInfo(allAccounts)
  debug('done')
  
  async function doSponsors() {
    debug('Sponsoring...')  
    for(let i = 0; i < 7; ++i) {
      const w = await wallet.load(-1)
      allAccounts[i] = w
      debug(`Sponsoring ben-${i}`)
      await sponsor(main, w)
      await showInfo({f1, main, w})
    }
  }

  async function showUsers() {
    let addr = BigInt((await f1.runmethod('get_first_user'))[1])
    if(addr > -1n) {
      do {
        const info = (await f1.runmethod('get_user_info', addr))[1].split(',')
        debug({addr, num_bens: Number(info[0])})
        addr = BigInt((await f1.runmethod('get_next_user', addr))[1])

      } while(addr > -1n)
    } else {
      debug('no users')
    }
  }

  async function showInfo(obj:Record<string, AAccount>) {
    for(let id in obj) {
      const acc = obj[id]
      debug(id, acc.naddr, await acc.info())
    }
  }

  async function giftAndDeploy(obj:Record<string, AAccount>, amt:number) {
    for(let id in obj) {
      const acc = obj[id]
      debug(`gifting ${id}`) 
      await acc.gift(amt, debug)
      debug(`deploying ${id}`) 
      await acc.deploy()      
    }
  }

  async function sponsor(w1:WalletAccount, w2:WalletAccount) {
    
    /*
      Bot would use its own faucet instance
    */
    const botsig = await f1.botSignAddress(w2.naddr, signers.bot)

    /*
      Client would use its own faucet instance
    */
    const msgBody = await f1.genSponsorshipMsgBody(w2.naddr, botsig) 
    const amt = config.sponsorshipPrice
    await w1.send(f1.naddr, amt, msgBody)

  }

}