#!/usr/local/bin/node --experimental-specifier-resolution=node

import {TscwJob} from './dev'
import {spawn, ChildProcessWithoutNullStreams} from 'child_process'
import createDebug from 'debug'

const debug = createDebug('tsmon')
const command = getCommand()
const args = process.argv.slice(3)
let job:ChildProcessWithoutNullStreams
let buildId = 0

const watch = new TscwJob('tscw', '.')
console.log('\u001bc')
watch.on('output', (fd, chunk) => {
  const lines = chunk.split('\n')

  for(let line of lines) {
    line = line.replace(/\u001bc/,'')
    if(line) watch.debug(line)
  }
})
watch.on('buildStarted', () => {      
  // console.log('\u001bc')   
  if(job) job.kill('SIGKILL')
  console.log('\n')
  debug(`# Build ${++buildId} ##################################`)
   
})     
 
watch.on('buildFinished', numErrors => { 

  console.log()
  debug(`running ${command} ${args.join(' ')}`)
  console.log()
  if(!numErrors) {

    job = spawn(command, args, {stdio:'inherit'})
  }
})

function getCommand() {
  const command = process.argv[2]
  if(!command) {
    debug('no command given, shutting down...')
    process.exit()
  }
  return command
}