import { AAccount, Environment, LiteClient, contracts, createDebug } from './sdk/njs'

const debug = createDebug('test')
const lc = new LiteClient
const env = new Environment(lc)

Environment.handleUnhandledRejections()

run()

async function run() {
  
  const contract = new contracts.Util.Contract(env)  
  const {FaucetV3} = contracts
  const acc = await contract.load(0)
  console.log('code:\n', await acc.getCode())
  debug('gifting...')
  await acc.gift(3)
  debug('deploying...')
  await acc.deploy()
  await showInfo(acc)
  
  const signers = await FaucetV3.Util.loadSigners()
  debug('sending test message')
  await acc.sendTestMessage(signers.master)
  debug('debugInfo: ' + await acc.getDebugInfo())
  debug('done')

  /*setInterval(async ()=>{
    await showInfo(acc)
    await debug('seqno: ' + await acc.seqno())
  }, 2000)*/

  async function showInfo(acc:AAccount) {
    debug(acc.naddr, {info: await acc.info()})
  }

}