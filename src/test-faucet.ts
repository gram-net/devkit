import { ADDR_GIFTER, AAccount, Environment, LiteClient, contracts, createDebug, until, hex2str } from './sdk/njs'

const debug = createDebug('test')
const lc = new LiteClient
// const lc:any = null
const env = new Environment(lc)

Environment.handleUnhandledRejections()

run()

async function run() {
  const {Faucet, Wallet} = contracts 
  
  //debug({code: await env.compileFunc(contracts.Faucet.CODE, true)})
  //process.exit()
  const {PRICE_SPONSORSHIP} = Faucet
  const faucet = new Faucet.Contract(env)  
  const wallet = new Wallet.Contract(env)
  const f1 = await faucet.load(0)
  const w1 = await wallet.load(0)
  const w2 = await wallet.load(0)
  const w3 = await wallet.load(0)
  
  debug('gifting f1')
  await f1.gift(20, debug)
  debug('deploying f1')
  await f1.deploy()
  
  debug('registering w1')
  await f1.register(w1.pkey, w1.naddr)
  
  debug('gifting w1')
  await w1.gift(20, debug)
  debug('deploying w1')
  await w1.deploy()
  const info = await f1.info()
  debug('sending sponsorship of w2')
  await w1.send(f1.naddr, PRICE_SPONSORSHIP, f1.createIntMsgBody(w1.pkey, w2.pkey, w2.naddr, "w2"))
  debug('sending sponsorship of w3')
  await w1.send(f1.naddr, PRICE_SPONSORSHIP, f1.createIntMsgBody(w1.pkey, w3.pkey, w3.naddr, "w3"))
  await f1.waitInfoChange('balance', info.balance, debug)
  await showInfo({f1, w1, w2})
  debug(await f1.get_info())
  let next_pending = await f1.get_pending()
  debug({next_pending})
  debug('invalidating')
  await f1.invalidate(next_pending.cpkey)
  next_pending = await f1.get_pending()
  debug({next_pending})
  debug('validating')
  await f1.validate(next_pending.cpkey)
  debug(await f1.get_info())
  
  await showInfo({f1, w1, w2, w3})
  debug('ticktock')
  const balance = (await w1.info()).balance
  await f1.ticktock()
  await showInfo({f1, w1, w2, w3})
    
  for(let pk = 1n; pk = await f1.get_next_user_pk(pk); ){
    debug({pk, payment: await f1.get_user_payment(pk)})
  }
  await w1.waitInfoChange('balance', balance, debug)
  await showInfo({f1, w1, w2, w3})
  debug('done')
  async function showInfo(obj:Record<string, AAccount>) {
    for(let id in obj) {
      const acc = obj[id]
      debug(id, acc.naddr, await acc.info())
    }
  }

}