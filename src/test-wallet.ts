import { AAccount, Environment, LiteClient, contracts, createDebug } from './sdk/njs'

const debug = createDebug('test')
const lc = new LiteClient
const env = new Environment(lc)

Environment.handleUnhandledRejections()

run()

async function run() {
  
  const walletContract = new contracts.Wallet.Contract(env)  
  const acc1 = await walletContract.load(0)
  const acc2 = await walletContract.load(0)

  debug({acc1})
  await showInfo(acc1)
  await acc1.gift(3, debug)  
  await showInfo(acc1)
  await acc1.deploy()
  await showInfo(acc1)
  await acc1.send(acc2.naddr, 1.5)  
  await showInfo(acc1)
  await showInfo(acc2)
  await acc2.waitInfoChange('balance', 0, debug)
  await showInfo(acc2)
  await acc2.deploy()
  await showInfo(acc2)
  debug('done')

  async function showInfo(acc:AAccount) {
    debug(acc.naddr, {info: await acc.info()})
  }

}