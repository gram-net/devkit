import { Environment, LiteClient, Signer, bytes2sl } from './sdk/njs'
import { FaucetV3 } from './sdk/com/all-contracts'
import fs from 'fs'

const fsp = fs.promises
const FN_OUT = process.argv[2] || 'sandbox/faucet-s1-zs-inc.fif'
const FN_CONFIG = process.argv[3] || 'sandbox/faucet-s1-config.json'

const env = new Environment(new LiteClient)

Environment.handleUnhandledRejections()

run()

async function run() {
  // await reset()
  const {master_pk, bot_pk, code} = await getConfig()
  const fasm = await env.compileFunc(code)
  console.log('fasm:\n', fasm);
  const content = `
    ${fasm} constant FAUCET_S1_CODE
    <b 0 32 u, ${bytes2sl(master_pk)} s, ${bytes2sl(bot_pk)} s, null dict, <b b> ref, b> constant FAUCET_S1_DATA
  `
  await fsp.writeFile(FN_OUT, content)
  env.dispose()
}

async function reset() {
  await fsp.unlink(FN_CONFIG)
}

async function getConfig() {
  
  let json

  try {
    json = (await fsp.readFile(FN_CONFIG)).toString()
    console.log('loaded from config file')
  } catch(e) {
    await FaucetV3.Util.genNewConfig(FN_CONFIG)
    json = (await fsp.readFile(FN_CONFIG)).toString()
    console.log('created new config file')
  }
  
  const {master_pk, bot_pk, sponsorshipPrice, maxBens, payBase} = JSON.parse(json)

  return new FaucetV3.Config(
    Uint8Array.from(Buffer.from(master_pk, 'base64')),
    Uint8Array.from(Buffer.from(bot_pk, 'base64')),
    sponsorshipPrice,
    maxBens,
    payBase
  )
  
}