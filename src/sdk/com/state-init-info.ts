export class StateInitInfo {
  
  codeCell:string
  
  dataCell:string
  
  libDict:string
  
  constructor(codeCell:string='null', dataCell:string='null', libDict:string='null') {
    Object.assign(this, {codeCell, dataCell, libDict})
  }

}