import {
  AAccount,
  bytes2sl,
  Signer,
  nowSeconds,
  until
} from '../../com'

import { Config } from './faucet-v3-config'
import * as Util from './faucet-v3-util'

import fs from 'fs'
import { AEnvironment } from '../aenvironment'

const fsp = fs.promises

export {Config, Util}

export class Account extends AAccount {

  async seqno() {
    return Number((await this.runmethod('seqno'))[1])
  }

  async waitSeqnoChange(oldSeqno:number) {
    await until(async ()=>{
      const newSeqno = await this.seqno()
      console.log({oldSeqno, newSeqno})
      return newSeqno !== oldSeqno
    })
  }

  async getDebugInfo() {
    const info = ((await this.runmethod('get_debug_info'))[1]).split(' ')
    console.log({info})
    return {
      seqno: Number(info[0]),
      bits: Number(info[1]),
      refs: Number(info[2])
    }
  }

  async botSignAddress(addr:string, bot:Signer) {
    return bot.sign((await this.fs.parseAddr(addr)).bytes)
  }

  async genSponsorshipMsgBody(benAddr:string, botSig:Uint8Array) {
    return `<b ${bytes2sl((await this.fs.parseAddr(benAddr)).bytes)} s, ${bytes2sl(botSig)} s, b>` 
  }

  async sendOp(opCode:number, opBody='') {
    const {fs} = this
    const seqno = await this.seqno()
    const msgBody = `<b ${seqno} 32 u, ${nowSeconds() + 10} 32 u, <b ${opCode} 8 u, ${opBody} b> ref, b>`
    const hash = await fs.hash(msgBody)
    await this.sendfile(await fs.emsgBoc(this.naddr, `<b ${bytes2sl(this.signer.sign(hash))} s, ${msgBody} ref, b>`))
    await this.waitSeqnoChange(seqno)
  }

  sendOp_Reset() {
    return this.sendOp(0)
  }

  sendOp_NewBotPubKey(newPubKey:Uint8Array) {
    return this.sendOp(1, `${bytes2sl(newPubKey)} s,`)
  }

}

export async function createAccount(env:AEnvironment, naddr:string) {
  const {bca} = env
  const fs = await env.createFiftSession()
  const account = new Account
  Object.assign(account, {env, bca, fs, workchain:-1, naddr})
  return account
}
