import {
  AAccount,
  GContract,
  StateInitInfo,
  bytes2sl,
  bytes2hex,
  hex2str,
  nowSeconds,
  until
} from '../../com'

const GRAM = 1000000000
const PAY_BASE = 2n
const MAX_LEVEL = 31n
const MAX_PAY = PAY_BASE ** MAX_LEVEL
const LDKS = log2bigint(MAX_LEVEL) // User->LevelDict->keySize

function log2bigint(b:bigint) {
  let i = 0n
  while(b) { ++i; b = b >> 1n }
  return i
}

export const PRICE_SPONSORSHIP = 1.073741824

class Account extends AAccount {

  createIntMsgBody(ppkey:Uint8Array, cpkey:Uint8Array, addr:string, tid:string ) {
    return `<b 0x${bytes2hex(ppkey)} 256 u, 0x${bytes2hex(cpkey)} 256 u, "${addr}" $>smca, <b "${tid}" $, b> ref, b>`
  }

  async sendExtMsg(ops:string) {
    const {fs} = this
    const seqno = await this.seqno()
    await fs.write(`<b ${seqno} 32 u, ${nowSeconds() + 10} 32 u, ${ops} b> temp1 !`)
    const sig = await this.sig('temp1 @')
    await this.sendfile(await fs.emsgBoc(this.naddr, `<b ${bytes2sl(sig)} s, temp1 @ ref, b>`))
    await until(async ()=>{
      const nseqno = await this.seqno()
      console.log({nseqno, seqno})
      return nseqno !== seqno
    })
  }

  register(pkey:Uint8Array, addr:string) {
    return this.sendExtMsg(`0 2 u, ${'0x'+bytes2hex(pkey)} 256 u, "${addr}" $>smca,`)
  }

  invalidate(pkey:bigint) {
    return this.sendExtMsg(`1 2 u, ${pkey} 256 u,`)
  }

  validate(pkey:bigint) {
    return this.sendExtMsg(`2 2 u, ${pkey} 256 u,`)
  }

  ticktock() {
    return this.sendExtMsg(`3 2 u,`)
  }

  get_user_payment(pk:bigint) {
    return this.runmethod('get_user_payment', pk);
  }

  async get_next_user_pk(pk:bigint) {
    const result = await this.runmethod('get_next_user_pk', pk) 
    return BigInt(result[1])
  }

  async get_info() {
    return await this.runmethod('get_info')
  }

  async get_pending() {
    const result = (await this.runmethod('get_pending'))[1].split(' ')
    return {
      cpkey: BigInt(result[0]),
      tid: hex2str(result[1].match(/{[0-9A-F]{4}([0-9A-F]*)}/)[1])
    }
  }

  async seqno() {
    return Number((await this.runmethod('seqno'))[1])
  }

  async deploy() {
    const {fs} = this
    await fs.write(`<b 0 32 u, ${nowSeconds() + 10} 32 u, b> temp1 !`)
    const sig = await this.sig('temp1 @')
    await this.sendfile(await fs.emsgBoc(this.naddr, `<b ${bytes2sl(sig)} s, temp1 @ ref, b>`, 'state_init'))
    await this.waitInfoChange('state', 'empty')
    await this.waitInfoChange('state', 'uninit')
  }

}

export class Contract extends GContract<Account> {
  
  async createAccount() { return new Account }

  async init(account:Account) {
    const {fs} = account
    fs.write('variable temp1')
  }

  async getStateInitInfo(account:Account) {
    const {env, signer} = account
    return new StateInitInfo(await env.compileFunc(CODE), `<b 0 32 u, ${bytes2sl(signer.pkey)} s, null dict, null dict, 0 64 u, b>`)
  }
  
}

export const CODE = `

_ unpack_data() {
  var ds = get_data().begin_parse();
  return (
    ds~load_uint(32),
    ds~load_uint(256),
    ds~load_dict(),
    ds~load_dict()
  );
}

() save_data(seqno, pk, pending, users) impure {
  set_data(begin_cell()
    .store_uint(seqno, 32)
    .store_uint(pk, 256)
    .store_dict(pending)
    .store_dict(users)
  .end_cell());
}

_ unpack_user(slice user) {
  return (
    user~load_dict(),
    user~load_uint(256),
    user~load_bits(267)
  );
}

(cell, ()) ~set_user(cell users, int pk, cell dl, int s_pk, slice addr) {
  users~udict_set_builder(256, pk, begin_cell()
    .store_dict(dl)
    .store_uint(s_pk, 256)
    .store_slice(addr)
  );
  return(users, ());
}

(cell, ()) ~inc_downline(cell users, int pk, int l) {
  if(pk > 0) {
    if(l >= 0) {
      var (user, found) = users.udict_get?(256, pk);
      var (dl, s_pk, addr) = unpack_user(user);
      var (s, found) = dl.udict_get?(${LDKS}, l);
      var v = found ? s.preload_uint(64) + 1 : 1;
      dl~udict_set_builder(${LDKS}, l, begin_cell().store_uint(v, 64));
      users~set_user(pk, dl, s_pk, addr);
      users~inc_downline(s_pk, l - 1);
    }
  }
  return (users, ());
}

(cell, ()) ~register(cell users, int pk, int s_pk, slice addr) {
  users~set_user(pk, new_dict(), s_pk, addr);
  users~inc_downline(s_pk, ${MAX_LEVEL});
  return(users, ());
}

int calc_payment_dl(cell dl, int payment, int level) {
  if(level >= 0) {
    var (count, found) = dl.udict_get?(${LDKS}, level);
    if(found) {
      payment = calc_payment_dl(dl, payment, level - 1) + (count.preload_uint(64) * (1 << level));
    }
  }
  return payment;
}

int calc_payment(cell dl) {
  if(dl.null?()) {
    return ${MAX_PAY} >> 10;
  } else {
    return calc_payment_dl(dl, ${MAX_PAY}, ${MAX_LEVEL}) >> 10;
  }
}

() recv_internal(int msg_value, cell in_msg_cell, slice in_msg) impure {
  
  throw_unless(100, msg_value == ${PRICE_SPONSORSHIP * GRAM}); ;; price = 2^30 nanogram = 1.073741824 GRAM
  var (seqno, pubkey, pending, users) = unpack_data();
  var s_pk = in_msg~load_uint(256);
  var (user, exists) = users.udict_get?(256, s_pk);
  throw_unless(101, exists);
  var pk = in_msg~load_uint(256);
  var (user, exists) = users.udict_get?(256, pk);
  throw_if(102, exists);
  pending~udict_set_builder(256, pk, begin_cell()
    .store_uint(s_pk, 256)
    .store_slice(in_msg~load_bits(267))
    .store_ref(in_msg~load_ref())
  );
  save_data(seqno, pubkey, pending, users);

}

() recv_external(slice is) impure {
  var (signature, mc) = (is~load_bits(512), is.preload_ref());
  var ds = get_data().begin_parse();
  var (seqno, pubkey, pending, users) = unpack_data();
  throw_unless(200, check_signature(mc.cell_hash(), signature, pubkey));
  var ms = mc.begin_parse();
  var m_seqno = ms~load_uint(32);
  var m_valid_until = ms~load_uint(32);
  throw_unless(201, seqno == m_seqno);
  throw_unless(202, now() <= m_valid_until);
  accept_message();
  if(m_seqno) {
    var op = ms~load_uint(2);
    if(op == 0) { ;; register
      var pk = ms~load_uint(256);
      var (user, found) = users.udict_get?(256, pk);
      throw_if(203, found);
      users~register(pk, 0, ms~load_bits(267));
    } elseif(op == 1) { ;; invalidate
      pending~udict_delete?(256, ms~load_uint(256));
    } elseif(op == 2) { ;; validate
      var pk = ms~load_uint(256);
      var (pending_entry, found) = pending~udict_delete_get?(256, pk);
      throw_unless(205, found);
      var (user, found) = users.udict_get?(256, pk);
      throw_if(206, found);
      var s_pk = pending_entry~load_uint(256);
      users~register(pk, s_pk, pending_entry~load_bits(267));      
    } elseif(op == 3) {
      var pk = 0;
      var found = 0;
      do {
        (pk, var user, found) = users.udict_get_next?(256, pk);
        if(found) {
          var (dl, s_pk, addr) = unpack_user(user);
          var payment = calc_payment(dl);
          send_raw_message(begin_cell().store_uint(0x10, 6)
            .store_slice(addr)
            .store_grams(payment)
            .store_uint(0, 1 + 4 + 4 + 64 + 32 + 1 + 1)
            .store_uint(0, 32)
            .end_cell(), 3);
        }
      } until (~ found);
    }
  }
  save_data(m_seqno + 1, pubkey, pending, users);

}

int seqno() method_id {
  return get_data().begin_parse().preload_uint(32);
}

_ get_info() method_id {
  return unpack_data();
}

_ get_user_payment(int pk) method_id {
  var (seqno, pubkey, pending, users) = unpack_data();
  var(user, found) = users.udict_get?(256, pk);
  var(dl, s_pk, addr) = unpack_user(user);
  return calc_payment(dl);
}

_ get_next_user_pk(int pk) method_id {
  var (seqno, pubkey, pending, users) = unpack_data();
  var(pk, user, found) = users.udict_get_next?(256, pk);
  return found ? pk : 0;
}

_ get_pending() method_id {
  var (seqno, pubkey, pending, users) = unpack_data();
  var (cpkey, s, exists) = pending.udict_get_min?(256);
  if(exists) {
    return (cpkey, s~load_ref().begin_parse());
  } else {
    return (cpkey, null());
  }
}

`

