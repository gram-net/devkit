const {floor, log2} = Math

export class Config {
  
  readonly master_pk:Uint8Array
  
  readonly bot_pk:Uint8Array

  readonly sponsorshipPrice:number
  
  readonly maxBens:number
  
  readonly payBase:number

  readonly numBensBits:number
  
  readonly code:string

  constructor(master_pk:Uint8Array, bot_pk:Uint8Array, sponsorshipPrice=1.073741824, maxBens=5, payBase=500000000000) {
    
    const numBensBits = 1 + floor(log2(maxBens))
    const payPerBenefactor = floor(payBase / maxBens)
    const code = `

    (slice, int) dict_get?(cell dict, int key_len, slice key) asm(key dict key_len) "DICTGET" "NULLSWAPIFNOT";
    (slice, slice, int) dict_get_next?(cell dict, int key_len, slice pivot) asm(pivot dict key_len -> 1 0 2) "DICTGETNEXT" "NULLSWAPIFNOT2";
    (slice, slice, int) dict_get_min?(cell dict, int key_len) asm (-> 1 0 2) "DICTMIN" "NULLSWAPIFNOT2";
    
    _ unpack_data() {
      var ds = get_data().begin_parse();
      return (
        ds~load_uint(32), ;; seqno
        ds~load_uint(256), ;; pk - master public key
        ds~load_uint(256), ;; bot_pk - bot public key
        ds~load_dict(), ;; users
        ds~load_ref() ;; debug
      );
    }
    
    () save_data(int seqno, int pk, int bot_pk, cell users, cell debug) impure {
      set_data(begin_cell()
        .store_uint(seqno, 32)
        .store_uint(pk, 256)
        .store_uint(bot_pk, 256)
        .store_dict(users)
        .store_ref(debug)
      .end_cell());
    }
    
    () recv_internal(int msg_value, cell in_msg_cell, slice in_msg) impure {
      
      ;; ######################
      ;; Extract sender address
      var cs = in_msg_cell.begin_parse();
      var flags = cs~load_uint(4);  ;; int_msg_info$0 ihr_disabled:Bool bounce:Bool bounced:Bool
      if (flags & 1) {
        ;; ignore all bounced messages
        return ();
      }
      var s_addr = cs~load_msg_addr();
      var (wc, addr) = parse_var_addr(s_addr);
      throw_if(101, wc + 1);

      ;; ######################
      ;; Unpack data
      var (seqno, pk, bot_pk, users, debug) = unpack_data();      
      
      if(users.dict_empty?()) {
        users~dict_set_builder(256, addr, begin_cell().store_uint(0, ${numBensBits}));
      } else {
        var (ben_addr, sig) = (in_msg~load_bits(256), in_msg.preload_bits(512));
        throw_unless(102, check_data_signature(ben_addr, sig, bot_pk));
        var (user, found) = users.dict_get?(256, ben_addr);
        throw_if(103, found);
        var (user, found) = users.dict_get?(256, addr);
        throw_unless(104, found);
        var num_bens = user~load_uint(${numBensBits});
        throw_unless(105, num_bens < ${maxBens});
        users~dict_set_builder(256, ben_addr, begin_cell().store_uint(0, ${numBensBits}));
        users~dict_set_builder(256, addr, begin_cell().store_uint(num_bens + 1, ${numBensBits}));
        send_raw_message(begin_cell().store_uint(0x10, 6)
            .store_uint(0x4,3)
            .store_int(-1,8)
            .store_slice(addr)
            .store_grams(${payPerBenefactor})
            .store_uint(0, 1 + 4 + 4 + 64 + 32 + 1 + 1)
            .store_uint(0, 32)
            .end_cell(), 3);
        send_raw_message(begin_cell().store_uint(0x10, 6)
          .store_uint(0x4,3)
          .store_int(-1,8)
          .store_slice(ben_addr)
          .store_grams(${payBase})
          .store_uint(0, 1 + 4 + 4 + 64 + 32 + 1 + 1)
          .store_uint(0, 32)
          .end_cell(), 3);
      }
      save_data(seqno, pk, bot_pk, users, debug);
    } 
    
    () recv_external(slice msg) impure {
      var (seqno, pk, bot_pk, users, debug) = unpack_data();
      var sig = msg~load_bits(512);
      var body = msg.preload_ref().begin_parse();
      var hash = slice_hash(body);
      throw_unless(201, check_signature(hash, sig, pk));
      var (b_seqno, valid_until, op) = (body~load_uint(32), body~load_uint(32), body.preload_ref().begin_parse());
      throw_unless(202, seqno == b_seqno);
      throw_unless(203, now() <= valid_until);
      var op_code = op~load_uint(8);
      if(op_code == 0) {
        accept_message();
        users = new_dict();
      } else {
        if(op_code == 1) {
          accept_message();
          bot_pk = op.preload_uint(256);
        }
      }
      
      save_data(seqno + 1, pk, bot_pk, users, begin_cell().store_slice(msg).end_cell());
    }
    
    int seqno() method_id {
      return get_data().begin_parse().preload_uint(32);
    }

    int get_first_user() method_id {
      var (seqno, pk, bot_pk, users, debug) = unpack_data();
      var (addr, user, found) = users.udict_get_min?(256);
      if(found) {
        return addr;
      } else {
        return -1;
      }
    }

    _ get_user_info(int addr) method_id {
      var (seqno, pk, bot_pk, users, debug) = unpack_data();
      var (user, found) = users.udict_get?(256, addr);
      if(found) {
        var num_bens = user~load_uint(${numBensBits});
        return num_bens;
      } else {
        return -1;
      }
    }

    _ get_next_user(int addr) method_id {
      var (seqno, pk, bot_pk, users, debug) = unpack_data();
      var (addr, user, found) = users.udict_get_next?(256, addr);
      if(found) {
        return addr;
      } else {
        return -1;
      }
    }

    _ get_debug_info() method_id {
      var (seqno, pk, bot_pk, users, debug) = unpack_data();
      var ds = debug.begin_parse();
      var (bits, refs) = (slice_bits(ds), slice_refs(ds));
      return (seqno, bits, refs);
    }
    
    `

    Object.assign(this, {master_pk, bot_pk, sponsorshipPrice, maxBens, payBase, numBensBits, payPerBenefactor, code})

  }

}