import {
  AAccount,
  GContract,
  StateInitInfo,
  bytes2sl,
  nowSeconds,
  until
} from '../../com'

export class Account extends AAccount {
  
  async send(dest_addr:string, amount:number, body='<b "TAKE IT" $, b>', bounce=false, init?:string) {
    const {fs} = this
    const seqno = await this.seqno()
    const imsg = fs.imsg(dest_addr, body, amount, bounce, init)
    await fs.write(`<b ${seqno} 32 u, ${nowSeconds() + 10} 32 u, 3 8 u, ${imsg} ref, b> temp1 !`)
    const hash = await fs.hash('temp1 @')
    const sig = this.signer.sign(hash)
    await this.sendfile(await fs.emsgBoc(this.naddr, `<b ${bytes2sl(sig)} s, temp1 @ <s s, b>`))
    await until(async ()=>{
      return await this.seqno() !== seqno
    })
  }

  async seqno() {
    return Number((await this.runmethod('seqno'))[1])
  }

  async deploy() {
    const {fs} = this
    await fs.write(`<b 0 32 u, ${nowSeconds() + 10} 32 u, b> temp1 !`)
    const sig = await this.sig('temp1 @')
    await this.sendfile(await fs.emsgBoc(this.naddr, `<b ${bytes2sl(sig)} s, temp1 @ <s s, b>`, 'state_init'))
    await this.waitInfoChange('state', 'empty')
    await this.waitInfoChange('state', 'uninit')
  }

}

export class Contract extends GContract<Account> {
  
  async createAccount() { return new Account }

  async init(account:Account) {
    const {fs} = account
    await fs.write('variable temp1')
  }

  async getStateInitInfo(account:Account) {
    const {env, signer} = account
    return new StateInitInfo(await env.compileFunc(CODE), `<b 0 32 u, ${bytes2sl(signer.pkey)} s, b>`)
  }
  
}

const CODE = `
;; Simple wallet smart contract

() recv_internal(slice in_msg) impure {
  ;; do nothing for internal messages
}

() recv_external(slice in_msg) impure {
  var signature = in_msg~load_bits(512);
  var cs = in_msg;
  var (msg_seqno, valid_until) = (cs~load_uint(32), cs~load_uint(32));
  throw_if(35, valid_until <= now());
  var ds = get_data().begin_parse();
  var (stored_seqno, public_key) = (ds~load_uint(32), ds~load_uint(256));
  ds.end_parse();
  throw_unless(33, msg_seqno == stored_seqno);
  throw_unless(34, check_signature(slice_hash(in_msg), signature, public_key));
  accept_message();
  cs~touch();
  while (cs.slice_refs()) {
    var mode = cs~load_uint(8);
    send_raw_message(cs~load_ref(), mode);
  }
  cs.end_parse();
  set_data(begin_cell().store_uint(stored_seqno + 1, 32).store_uint(public_key, 256).end_cell());
}

;; Get methods

int seqno() method_id {
  return get_data().begin_parse().preload_uint(32);
}

int get_public_key() method_id {
  var cs = get_data().begin_parse();
  cs~load_uint(32);
  return cs.preload_uint(256);
}
`