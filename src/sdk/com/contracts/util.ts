import {
  AAccount,
  GContract,
  StateInitInfo,
  bytes2sl,
  nowSeconds,
  until,
  Signer
} from '../../com'

export class Account extends AAccount {
  
  async seqno() {
    return Number((await this.runmethod('seqno'))[1])
  }

  async deploy() {
    const {fs} = this
    await this.sendfile(await fs.emsgBoc(this.naddr, `<b b>`, 'state_init'))
    await this.waitInfoChange('state', 'empty')
    await this.waitInfoChange('state', 'uninit')
  }

  async sendTestMessage(signer:Signer) {
    const msg = `<b 1 1 u, b>`
    const seqno = await this.seqno();
    this.signer = signer
    const sig = await this.sig(msg)
    await this.sendfile(await this.fs.emsgBoc(this.naddr, `<b ${seqno} 32 u, 1 8 u, ${bytes2sl(signer.pkey)} s, ${bytes2sl(sig)} s, ${msg} ref, b>`))
    await until(async ()=>{return (await this.seqno()) !== seqno})
  }

  async getDebugInfo() {
    return this.runmethod('get_debug')
  }

  async getCode() {
    return await this.env.compileFunc(CODE)
  }

}

export class Contract extends GContract<Account> {
  
  async createAccount() { return new Account }

  async init(account:Account) {
    
  }

  async getStateInitInfo(account:Account) {
    const {env} = account
    const VERSION = 2
    return new StateInitInfo(await env.compileFunc(CODE), `<b 0 32 u, <b ${Date.now()} 64 u, b> ref, b>`)
  }
  
}

const CODE = `
;; Utility contract

(slice, int) dict_get?(cell dict, int key_len, slice key) asm(key dict key_len) "DICTGET" "NULLSWAPIFNOT";
(slice, slice, int) dict_get_next?(cell dict, int key_len, slice pivot) asm(pivot dict key_len -> 1 0 2) "DICTGETNEXT" "NULLSWAPIFNOT2";
(slice, slice, int) dict_get_min?(cell dict, int key_len) asm (-> 1 0 2) "DICTMIN" "NULLSWAPIFNOT2";

_ unpack_data() {
  var ds = get_data().begin_parse();
  return (
    ds~load_uint(32),
    ds~load_ref()
  );
}

() save_data(int seqno, cell debug) impure {
  set_data(begin_cell()
    .store_uint(seqno, 32)
    .store_ref(debug)
  .end_cell());
}

() recv_internal(slice in_msg) impure {
  ;; do nothing for internal messages
}

() recv_external(slice in_msg) impure {
  var (seqno, debug) = unpack_data();
  var new_debug = debug;
  if(seqno) {
    accept_message();
    new_debug = begin_cell().store_slice(in_msg).end_cell();
    var im_seqno = in_msg~load_uint(32);
    throw_unless(101, im_seqno == seqno);
  } else {
    accept_message();    
  }
  

  save_data(seqno + 1, new_debug);
}

;; Get methods

int seqno() method_id {
  var (seqno, debug) = unpack_data();
  return seqno;
}

_ get_debug() method_id {
  var (seqno, debug) = unpack_data();
  var ds = debug.begin_parse();
  var bits = slice_bits(ds);
  var refs = slice_refs(ds);
  return (seqno, bits, refs);
}

`