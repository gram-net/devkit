import {
  Signer
} from '../../com'

import nacl from 'tweetnacl'
import fs from 'fs'
import { Config } from './faucet-v3-config'

const fsp = fs.promises

export const FN_MASTER_SEED = 'sandbox/faucet-s1-master.seed'
export const FN_BOT_SEED = 'sandbox/faucet-s1-bot.seed'
export const FN_CONFIG = 'sandbox/faucet-s1-config.json'

export function randomSeed2File(fn:string) {
  return fsp.writeFile(fn, nacl.randomBytes(32))
}

export async function loadSignerFromSeedFile(fn:string) {
  return Signer.fromSeed(new Uint8Array(await fsp.readFile(fn)))
}

export async function loadSigners(fn_master_seed=FN_MASTER_SEED, fn_bot_seed=FN_BOT_SEED) {
  
  return {
    master: await loadSignerFromSeedFile(FN_MASTER_SEED),
    bot: await loadSignerFromSeedFile(FN_BOT_SEED)
  }

}

export function genRandomSeeds(fn_master_seed=FN_MASTER_SEED, fn_bot_seed=FN_BOT_SEED) {
  return Promise.all([randomSeed2File(fn_master_seed), randomSeed2File(fn_bot_seed)])
}

export async function loadConfig(fn=FN_CONFIG) {
  const json = (await fsp.readFile(FN_CONFIG)).toString()
  const {master_pk, bot_pk, sponsorshipPrice, maxBens, payBase} = JSON.parse(json)
  return new Config(
    Uint8Array.from(Buffer.from(master_pk, 'base64')),
    Uint8Array.from(Buffer.from(bot_pk, 'base64')),
    sponsorshipPrice,
    maxBens,
    payBase
  )
}

export async function genNewConfig(fn=FN_CONFIG) {
  await genRandomSeeds()
  const signers = await loadSigners()
  const config = new Config(signers.master.pkey, signers.bot.pkey)
  const {sponsorshipPrice, maxBens, payBase} = config
  const jobj = {
    master_pk: Buffer.from(config.master_pk).toString('base64'),
    bot_pk: Buffer.from(config.bot_pk).toString('base64'),
    sponsorshipPrice,
    maxBens,
    payBase
  }
  const json = JSON.stringify(jobj, null, 2)
  await fsp.writeFile(fn, json)
}