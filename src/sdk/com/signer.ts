import nacl from 'tweetnacl'

import { bytes2sl } from '../com'

const keyPair = nacl.sign.keyPair
const sign = nacl.sign.detached

export class Signer {
  
  pkey:Uint8Array
  
  skey:Uint8Array

  constructor(pkey:Uint8Array, skey:Uint8Array) {
    this.pkey = pkey
    this.skey = skey
  }

  static fromSeed(seed:Uint8Array) {
    const kp = keyPair.fromSeed(seed)
    return new Signer(kp.publicKey, kp.secretKey)
  }

  static fromSecretKey(skey:Uint8Array) {
    const kp = keyPair.fromSecretKey(skey)
    return new Signer(kp.publicKey, kp.secretKey)
  }

  static get random() {
    const kp = keyPair()
    return new Signer(kp.publicKey, kp.secretKey)
  }

  sign(arr:Uint8Array) {
    return sign(arr, this.skey)
  }

  sign2sl(arr:Uint8Array) {
    return bytes2sl(this.sign(arr))
  }

}