import {AAccount, StateInitInfo, hex2bytes, ADDR_GIFTER} from '../com'

class FiftSessionError extends Error {}

export abstract class AFiftSession {
  
  async parseAddr(addr:string) {
    try {
      const result = (await this.write(`"${addr}" $>smca 2drop swap . cr . cr "${addr}" $>smca 2drop nip <b swap 256 u, b> <s 32 B@ Bx. cr`)).split('\n')
      const wc = Number(result[0])
      const uint = BigInt(result[1])
      const bytes = hex2bytes(result[2])
      return {wc, uint, bytes}
    } catch(e) {
      throw new FiftSessionError(`Invalid address, could not parse: ${addr}`)
    }
  }

  async init(
    account:AAccount,    
    stateInitInfo:StateInitInfo
  ) {
    const lines = `
      
      {
        dup null? { drop b{0} s, } { swap b{11} s, swap ref, } cond
      } : state_init,
      
  
      ${account.workchain}
      <b
        b{0011} s,
        ${stateInitInfo.codeCell} ref,
        ${stateInitInfo.dataCell} ref,
        ${stateInitInfo.libDict} dict,
      b> dup constant state_init hashu
      2dup 2constant addr
      2dup 4 smca>$ type space
      5 smca>$ type

    `
    const [baddr, naddr] = (await this.write(lines)).split(' ')
    Object.assign(account, {baddr, naddr})    
  }

  async hash(cell:string) {
    return hex2bytes(await this.write(`${cell} hashB Bx.`))
  }

  async boc(cell:string) {

    const bocBytes = await this.write(`${cell} 2 boc+>B Bx.`)
    const debug = await this.write(`${cell} <s csr.`)
    return hex2bytes(bocBytes)
  }
  
  async int(fif:string) {
    return parseInt(await this.write(fif))
  }
  
  msg(info:string, body:string, init:string='null') {
    return `<b ${info} <s s, ${init} state_init, b{1} s, ${body} ref, b>`
  }

  imsg(toAddr:string, body:string, amount:number, bounce:boolean=false, init?:string) {
    return this.msg(`<b
      b{01} s, // int_msg_info$0 ihr_disabled$1
      b{${bounce ? 1 : 0}} s,
      b{000} s, // bounced$0 src:addr_none$00
      "${toAddr}" $>smca,
      ${Math.floor(1000000000 * amount)} Gram, null dict,
      0 Gram,
      0 Gram,
      0 64 u,
      0 32 u,
    b>`, body, init)
  } 

  emsg(toAddr:string, body:string, init?:string) {
    return this.msg(`<b
      b{10} s, // ext_in_msg_info$00
      b{00} s, // src:addr_none$00
      "${toAddr}" $>smca, // dest
      0 Gram, // import_fee
    b>`, body, init)
  }

  emsgBoc(toAddr:string, body:string, init?:string) {
    return this.boc(this.emsg(toAddr, body, init))
  }

  giftMsg(toAddr:string, amount:number, seqno:any) {
    const imsg = this.imsg(toAddr, `<b "GIFT" $, b>`, amount)
    const body = `<b ${parseInt(seqno)} 32 u, 1 8 u, ${imsg} ref, b>`
    return this.emsg(ADDR_GIFTER, body)
  }

  giftBoc(toAddr:string, amount:number, seqno:any) {
    return this.boc(this.giftMsg(toAddr, amount, seqno))
  }

  abstract async write(input:string):Promise<string>

  abstract async dispose():Promise<void>

}
