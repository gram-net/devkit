import {
  AEnvironment,
  ABlockchainAccess,
  AFiftSession,
  Signer,
  ADDR_GIFTER,
  sleep,
  noop
} from '../com'

export abstract class AAccount {
  
  env:AEnvironment

  bca:ABlockchainAccess
  
  fs:AFiftSession
  
  signer:Signer

  workchain:number
    
  naddr:string
  
  baddr:string

  pkey:Uint8Array
  
  async sendfile(content:Uint8Array) {
    this.bca.sendfile(content)
  }

  async runmethod(method:string, ...args:any[]) {
    return this.bca.runmethod(this.naddr, method, ...args)
  }

  async waitInfoChange(param:string, oval:any, oncheck = noop) {
    let info:any
    for(;;) {
      info = await this.info()
      const isEqual = (info[param] === oval)
      oncheck({param, oval, nval: info[param], isEqual})
      if(!isEqual) return 
      await sleep(2500) 
    }
  }

  async sig(cell:string) {
    return this.signer.sign(await this.fs.hash(cell))
  }

  async gift(amount:number, oncheck=noop) {
    const {balance} = await this.info()
    const seqno = (await this.bca.runmethod(ADDR_GIFTER, 'seqno'))[1]
    await this.sendfile(await this.fs.giftBoc(this.naddr, amount, seqno))
    await this.waitInfoChange('balance', balance, oncheck)
  }

  async deploy() {}
  
  async info() {
    const result = await this.bca.getaccount(this.naddr)
    // console.log({result})
    let state = 'empty', balance = 0n, match
    if(!result.match(/account state is empty/)) {
      match = result.match(/state:account_([^\)]*)\)/)
      if(match) {
        state = match[1]
      } else {
        state = 'active'
      }
      match = result.match(/account balance is \(?([0-9]*)/)
      balance = BigInt(match[1])
    }
    return {state, balance}      
  }

}
