import { AAccount, AEnvironment, Signer, StateInitInfo } from '../com'

export abstract class GContract<Account extends AAccount> {
  
  env:AEnvironment
  
  constructor(env:AEnvironment) {
    this.env = env
  }
  
  async load(workchain:number, signer:Signer=Signer.random, ...args:any ) {
    const {env} = this
    const {bca} = env
    const fs = await this.env.createFiftSession()
    const account = await this.createAccount(...args)
    Object.assign(account, {env, bca, fs, workchain, signer, pkey: signer.pkey})
    await fs.init(account, await this.getStateInitInfo(account))
    await this.init(account)
    return account
  }

  async init(account:AAccount) {}

  abstract async createAccount(...args:any):Promise<Account>
  
  abstract async getStateInitInfo(account:Account):Promise<StateInitInfo>

}