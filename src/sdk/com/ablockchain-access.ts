import { AAccount, AFiftSession } from '../com'

export abstract class ABlockchainAccess {
  
  abstract dispose():Promise<void>
  
  abstract getaccount(address:string):Promise<string> 
  
  abstract sendfile(content:Uint8Array):Promise<void>
  
  abstract runmethod(address:string, method:string, ...args:any[]):Promise<string[]>
  
}
