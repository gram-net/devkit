export async function sleep(ms:number) {
  return new Promise(resolve=>setTimeout(resolve, ms))
}

export async function until(conditionCallback:()=>Promise<boolean>, max_checks=10, delay:number=2500) {
  while(--max_checks && ! await conditionCallback()) await sleep(delay)
  return max_checks
}

export function bytes2hex(a:Uint8Array) {
  return a.reduce((s, b) => s + (b > 15 ? b.toString(16) : '0' + b.toString(16)), '')
}

export function hex2bytes(h:string) {
  const l = h.length / 2
  const a = new Uint8Array(l)
  for(let i = 0; i < l; ++i) a[i] = parseInt(h.substr(i*2, 2), 16)
  return a
}

export function hex2str(h:string) {
  const l = h.length / 2
  let s = ''
  for(let i = 0; i < l; ++i) s += String.fromCharCode(parseInt(h.substr(i*2, 2), 16))
  return s
}

export function bytes2sl(a:Uint8Array) {
  return `x{${bytes2hex(a)}}`
}

export function nowSeconds() {
  return Math.floor(Date.now() / 1000)
}

export const noop = (...args:any[])=>{}

export function log2bigint(b:bigint) {
  let i = 0n
  while(b) { ++i; b = b >> 1n }
  return i
}