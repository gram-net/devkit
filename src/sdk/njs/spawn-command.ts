export class SpawnCommand {

  readonly program:string
  
  readonly args:string[]
  
  constructor(command:SpawnCommand|string) {
    if(command instanceof SpawnCommand) {
      Object.assign(this, command)
    } else {
      const split = command.split(/\s+/)
      this.program = split.shift()
      this.args = split
    }
  }

  toString():string { return `${this.program} ${this.args.join(' ')}` }

}