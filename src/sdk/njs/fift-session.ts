import { spawn, ChildProcessWithoutNullStreams } from 'child_process'
import { AFiftSession } from '../com'

class FiftSessionError extends Error {}
class FiftSessionWriteError extends Error {
  constructor(errmsg:string, input:string, output:string) {
    super(`${errmsg}\ninput:\n${input}\noutput:\n${output}`)
  }
}

export class FiftSession extends AFiftSession {
  
  cp:ChildProcessWithoutNullStreams

  constructor() {
    super()
    this.cp = spawn('fift', ['-i'])
    this.cp.on('exit', code => {
      if(code) throw new FiftSessionError('\n'+this.cp.stderr.read())
    })
  }  

  async write(oinput:string):Promise<string> {

    let input = ''
    
    const {output, errmsg} = await new Promise( resolve => {
      
      const {stdin, stdout, stderr} = this.cp
      let output = ''

      stdout.on('data', onout)
      stderr.on('data', onerr)
      
      const lines = oinput.split('\n')
      for(let line of lines) input += line.replace(/\/\/.*/,'').trim() + ' '
      
      stdin.write(input + '\n')
      
      function onout(out_chunk:any) {
        
        const s = out_chunk.toString()
        // console.log({s})
        if( !s.endsWith(' ok\n') ) {
          output += s
        } else {
          unlink()
          output += s.substr(0, s.length - 4)
          resolve({output, errmsg: ''})  
        }
        
      }
      
      function onerr(err_chunk:any) {
        // console.log({e: err_chunk.toString()})
        unlink() && resolve({output, errmsg: err_chunk.toString()})
      }

      function unlink() { return stdout.off('data', onout) && stderr.off('data', onerr) }
    })

    if(errmsg) throw new FiftSessionWriteError(errmsg, input, output)
    return output   

  }

  async dispose():Promise<void> {
    this.cp.kill('SIGKILL')
  }
  
}