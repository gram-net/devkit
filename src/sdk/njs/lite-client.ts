import { join, default as path } from 'path'
import { promises as fsp } from 'fs'
import { spawn, ChildProcessWithoutNullStreams } from 'child_process'

import nacl from 'tweetnacl'
import createDebug from 'debug'

import { ABlockchainAccess } from '../com'
import { SpawnCommand } from './spawn-command'

const debug = createDebug('njs/lite-client')

const TFN_LS_TMP = path.resolve(''+process.env.TFN_LS_TMP)
const TFN_LS_PUB = path.resolve(''+process.env.TFN_LS_PUB)

export class LiteClientError extends Error {
  constructor(client:LiteClient, msg:string) {
    super(`\n  command: ${client.command}\n  msg: ${msg}`)
  }
}

export class LiteClient extends ABlockchainAccess {

  maxQueryTime:number
  command:SpawnCommand
  idle:ChildProcessWithoutNullStreams[] = []

  constructor(command?:SpawnCommand|string, maxQueryTime=3000) {
    super()
    this.command = new SpawnCommand(command || this.defaultCommand)
    this.maxQueryTime = maxQueryTime
    console.log(''+this.command)
  }

  createWorker():Promise<ChildProcessWithoutNullStreams> {
    return new Promise((resolve, reject)=> {
      let log = ''
      const abortTimeout = setTimeout(()=>{
        cp.kill()
        reject(new LiteClientError(this, `worker init took too long\nlog:\n${log}`))
      }, this.maxQueryTime)
      const {command} = this
      const cp = spawn(command.program, command.args)
      const {stdout, stderr} = cp
      const ondata = (chunk:any) => {
        chunk += ''
        log += chunk
        let match = chunk.match(/\[([^\[]*Error[^\]]*)\]/)
        if(match) {
          cp.kill()
          return reject(new LiteClientError(this, chunk))
        }
        match = chunk.match(/latest masterchain block known to server is/) 
        if(match) {
          stdout.off('data', ondata)
          stderr.off('data', ondata)
          clearTimeout(abortTimeout)
          setTimeout(()=>resolve(cp), 100)
        }
      }
      stdout.on('data', ondata) 
      stderr.on('data', ondata) 
    })
  }

  last() {
    return this.query('last', (ctx:string, resolve:any, reject:any)=>{
      const match = ctx.match(/latest masterchain block known to server is (\(.*\))/)
      if(match) resolve(match[1])
    }, false)
  }

  async sendfile(content:Uint8Array) { 
    const fnBase = 't'+Buffer.from(nacl.randomBytes(15)).toString('base64').replace(/\//g,'-')
    const fn = join(TFN_LS_TMP, fnBase )
    await fsp.writeFile(fn, content)
    await this.query('sendfile ' + fn,(ctx:string, resolve:any, reject:any)=>{
      if(ctx.match(/external message status is 1/)) resolve()
    })    
  }

  async runmethod(address:string, method:string, ...args:any[]) {
    return <string[]> await this.query(`runmethod ${address} ${method} ${args.map(a=>a.toString().trim()).join(' ')}`,
      (ctx:string, resolve:any, reject:any)=>{
      let match
      match = ctx.match(/arguments:\s*\[([^\]]*)\]\s*result:\s*\[([^\]]*)\]/) 
      if(match) resolve([match[1].trim(), match[2].trim()])
    })
  }

  async getaccount(address:string) {
    return <string> await this.query(`getaccount ${address}`,
      (ctx:string, resolve:any, reject:any)=>{
      if(ctx.match(/account state is/)) {
        resolve(ctx)
      }      
    })
  }

  async query(q:string, handle:any, runLast:boolean=true) {
    // console.log({q})
    if(q !== 'last') await this.last()
    const w = this.idle.length ? this.idle.pop() : await this.createWorker()
    return new Promise((resolve, reject)=>{
      const client = this
      let log:string[] = []      
      const abortTimeout = setTimeout(()=>{
        reject(new LiteClientError(this, `query took too long => ${q}\nlog:\n${log.join('\nCHUNK:\n')}\n`))
      }, this.maxQueryTime)
      const release = () => { 
        clearTimeout(abortTimeout)
        w.stdout.off('data', ondata)
        w.stderr.off('data', ondata)
        this.idle.push(w)
      }
      const hresolve = (...args:any[]) => { release(); resolve(...args) }
      const hreject = (...args:any[]) => { release(); reject(...args) }
      w.stdout.on('data', ondata)
      w.stderr.on('data', ondata)
      // console.log({q})
      w.stdin.write(q+'\n', 'utf-8')
                  
      function ondata(ctx:any) {
        ctx += '';
        log.push(ctx);
        let match = ctx.match(/\[([^\[]*Error[^\]]*)\]/)
        if(match) reject(new LiteClientError(client, `error during query => ${q}\nerror: ${match[1]}\n`))
        handle(ctx, hresolve, hreject)
      }
      
    })
  }

  async dispose() {
    for(let w of this.idle) w.kill()
  }

  get defaultCommand():string {
    return `lite-client -a ${ process.env.TFN_LS_IP }:${ process.env.TFN_LS_PORT } -p ${ TFN_LS_PUB }`
  }

}