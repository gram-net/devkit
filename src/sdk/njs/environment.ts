import { spawn } from 'child_process'

import { AEnvironment } from '../com'
import { LiteClient } from './lite-client.js'
import { FiftSession } from './fift-session.js'

class FuncCompileError extends Error {
  constructor(src:string, errors:string) {
    super(errors)
  }
}

export class Environment extends AEnvironment {
  
  createFiftSessionImpl() {
    return new FiftSession
  }

  compileFuncImpl(src: string, addComments=false): Promise<string> {
    return new Promise( async (resolve, reject) => {
      const flags = '-IP' + (addComments ? 'S' : '')
      const stdout:string[] = []
      let errors:string = ''
      const cp = spawn('func', [flags])
      cp.stdout.on('data', (chunk:any) => { stdout.push(chunk.toString()) })
      cp.stderr.on('data', (chunk:any) => { errors+=chunk.toString() })
      cp.on('close', () => {
        if(errors.length) {
          throw new FuncCompileError(src, errors)
        } else {
          resolve(stdout.join(''))
        }
      })
      const lines = src.split('\n')
      for(let line of lines) cp.stdin.write(line+'\n')
      cp.stdin.end()
    })    
  }
  
  static handleUnhandledRejections() {
    process.on('unhandledRejection', error => {
      console.error('UnhandledRejection:')
      console.error(error)
      process.exit()
    })
  }

}