export const ADDR_GIFTER = 'Ef9mZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZse2'

export * from './com/aaccount'
export * from './com/ablockchain-access'
export * from './com/aenvironment'
export * from './com/afift-session'
export * from './com/gcontract'
export * from './com/signer'
export * from './com/state-init-info'
export * from './com/util'
export * as contracts from './com/all-contracts'

import createDebug from 'debug'
export {createDebug}
