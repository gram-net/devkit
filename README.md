# tfn-dev-kit

## Install

`yarn`

## Initialize

`yarn init:all`

Edit .env according to your environment

Note:
 path to TNF_LS_PUB needs to be absolute because I don't yet have a solution
 for getting the right cwd for the tsmon cli - thisPkg:bin:tsmon

## Develop

`yarn test`

## Faucet S1

Use `sandbox/test-zs.fif` in place of `gen-zerostate.fif`
This zs script `include`s another script `faucet-s1-zs-inc.fif` which can be generated with `yarn gen-faucet-s1-zs-inc`
This script depends on `sandbox/faucet-s1-config.json` which can be edited manually if you want to provide your own master and/or bot pubkey(s).

If you want to start clean, just delete `sandbox/faucet-s1-config.json` and it will generate a new one as well as the following 2 files:
`sandbox/faucet-s1-bot.seed`
`sandbox/faucet-s1-master.seed`

See `src/test-faucet-v3.ts` for an example of wallets/bot interaction with faucet contract.